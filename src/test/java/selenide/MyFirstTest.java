package selenide;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.PropertyLoader;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;

public class MyFirstTest extends BaseTest {



    @Test
    public void incorrectWebexLogin() {
        $(By.id("guest_signin_button")).click();
        $(By.id("IDToken1")).val("123");
        $(By.id("IDButton2")).shouldBe(Condition.visible).click();
        $(By.cssSelector("#nameContextualError1")).shouldBe(Condition.visible)
                .shouldHave(Condition.text("Enter a valid email address. Example: name@email.com"));

    }


    @Test
    public void correctWebexLogin() {

        $(By.id("guest_signin_button")).click();
        $(By.id("IDToken1")).val(PropertyLoader.loadProperty("email"));
        $(By.id("IDButton2")).shouldBe(Condition.visible).click();
        $$(By.cssSelector("#nameContextualError1")).shouldHaveSize(0);
        $(By.xpath("//span[contains(text(),'MyPortal Account (Alternative)')]")).click();
        $(By.cssSelector("#username")).val(PropertyLoader.loadProperty("email"));
        $(By.xpath("//input[@id='password']")).val(PropertyLoader.loadProperty("pass"));
        $(By.cssSelector("#submit")).shouldBe(Condition.visible).click();
        refresh();
//        Assert.assertTrue((url().equals("https://dtag.webex.com/webappng/sites/dtag/dashboard/home")),"Mentioned Link wasn't found. Current link is: " + url());
        $(By.xpath("//div[@class='pmr_card_c_title']")).shouldBe(Condition.visible)
                .shouldHave(Condition.text("Anna Burmatnova's Personal Room"));


    }
}
