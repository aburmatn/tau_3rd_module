package selenide;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeMethod;

import static com.codeborne.selenide.Selenide.open;

public class BaseTest {

    @BeforeMethod


    public void init() {
        Configuration.browser = "chrome";
        String url = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag";
        open(url);


    }
}