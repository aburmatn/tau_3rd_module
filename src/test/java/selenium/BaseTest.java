package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    WebDriver webDriver;
    
    @BeforeClass


    public void init() {

        webDriver= new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Java\\drivers\\Chrome\\chromedriver.exe");
        webDriver.get("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        webDriver.navigate().to("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
        @AfterClass

        public void stop() {
            webDriver.quit();
        }
}
