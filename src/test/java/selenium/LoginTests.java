package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LoginTests extends BaseTest {



    @Test
    public void incorrectWebexLogin(){
        webDriver.findElement(By.id("guest_signin_button")).click();
        webDriver.findElement(By.cssSelector("#IDToken1")).sendKeys("123");
        webDriver.findElement(By.id("IDButton2")).click();
        WebDriverWait wait = new WebDriverWait(webDriver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("nameContextualError1")));
        WebElement errorMessage = webDriver.findElement(By.id("nameContextualError1"));
        Assert.assertTrue(errorMessage.getText().equals("Enter a valid email address. Example: name@email.com"),"error message contains other text: " + errorMessage.getText());


    }


    @Test
    public void correctWebexLogin(){
        webDriver.findElement(By.id("guest_signin_button")).click();
        webDriver.findElement(By.cssSelector("#IDToken1")).sendKeys("anna.burmatnova@t-systems.com");
        webDriver.findElement(By.id("IDButton2")).click();
        webDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        webDriver.findElement(By.xpath("//div[2]/div[2]/div/main/div/div/form/div[1]/div[3]/div")).click();
        WebElement ciamLoginPage = webDriver.findElement(By.xpath("//h1[@class='text-center']"));
        Assert.assertTrue(ciamLoginPage.getText().equals("T-SSO Login"), "Wrong page redirect. Current page titel: " + ciamLoginPage.getText());
        webDriver.findElement(By.cssSelector("#username")).sendKeys("anna.burmatnova@t-systems.com");
        webDriver.findElement(By.xpath("//input[@id='password']")).sendKeys("************");
        webDriver.findElement(By.cssSelector("#submit")).click();
        WebElement webexHomePage = webDriver.findElement(By.xpath("//div[@class='pmr_card_c_title']"));
        Assert.assertTrue(webexHomePage.getText().equals("Anna Burmatnova's Personal Room"), "Redirect to the wrong page. Current page is: " + webexHomePage.getText());


    }



//    @Test
//    public void openWebexFF(){
//        System.setProperty("webdriver.gecko.driver", "C:\\Java\\drivers\\Gecko\\geckodriver.exe");
//        WebDriver webDriver = new FirefoxDriver();
//        webDriver.get("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
//        webDriver.navigate().to("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
//        webDriver.quit();
//    }

//    @Test
//    public void openWebexEdge(){
//        System.setProperty("webdriver.edge.driver", "C:\\Java\\drivers\\Edge\\msedgedriver.exe");
//        WebDriver webDriver = new EdgeDriver();
//        webDriver.get("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
//        webDriver.navigate().to("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
//        webDriver.quit();
//    }
}
