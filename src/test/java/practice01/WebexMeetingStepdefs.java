package practice01;

import com.codeborne.selenide.Condition;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PropertyLoader;

import static com.codeborne.selenide.Condition.appears;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;


public class WebexMeetingStepdefs {
    protected Logger log = LoggerFactory.getLogger(getClass());


    @Given("user is logged in")
    public void userIsLoggedIn() {
        open("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        log.info("page is: " + url());
        $(By.id("guest_signin_button")).waitUntil(appears, 10000);
        $(By.id("guest_signin_button")).click();
        $(By.cssSelector("[name=IDToken1]")).val(PropertyLoader.loadProperty("email"));
        $(By.cssSelector("[name=btnOK]")).click();
        log.info("page is: " + url());

        $(By.xpath("//span[contains(text(),'MyPortal Account (Alternative)')]")).click();
        $(By.cssSelector("#username")).val(PropertyLoader.loadProperty("email"));
        $(By.xpath("//input[@id='password']")).val(PropertyLoader.loadProperty("pass"));
        $(By.cssSelector("#submit")).shouldBe(Condition.visible).click();
    }

    @When("we go to meeting creationForm")
    public void weGoToMeetingCreationForm() {

        $(By.xpath("//*[@class='pmr_card_c_title']/h1")).shouldBe(Condition.visible)
                .shouldHave(Condition.text("Anna Burmatnova's Personal Room"));
        $(By.cssSelector("[title='Meetings']")).click();
        $(By.cssSelector("#scheduleEntry")).shouldBe(Condition.visible).click();
        $(By.cssSelector("#topic")).shouldBe(Condition.exist);
        $(By.cssSelector("#topic")).sendKeys(Keys.CONTROL + "A");
        $(By.cssSelector("#topic")).sendKeys(Keys.BACK_SPACE);
        $(By.cssSelector("#topic")).shouldHave(exactText(""));
        $(By.cssSelector("#topic")).val("super_puper_Meeting");
        $(By.cssSelector("#topic")).sendKeys(Keys.TAB);
        $(By.cssSelector("#topic")).shouldHave(Condition.text("super_puper_Meeting"));
    }

    @And("we fill all necessary data and submit meeting")
    public void weFillAllNecessaryDataAndSubmitMeeting() {

        $(By.xpath("//a[@id='scheduleTimeTrigger']/i")).click();
        $(By.xpath("//*[@aria-label='Tuesday April 20, 2021']")).click();
        $(By.cssSelector("#scheduleDone")).click();
        $(By.cssSelector("#invite_attendee_input")).val("elena.sergeeva@t-systems.com");
        $(By.cssSelector("#scheduleEdit")).click();
    }

    @Then("meeting is successfully created")
    public void meetingIsSuccessfullyCreated() {
        log.info("page is: " + url());
    }
}
