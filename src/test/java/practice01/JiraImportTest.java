package practice01;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/practice01.resources/post_results_jira_xray.feature",
        plugin = {"pretty"}
)
public class JiraImportTest {
}
