package practice01;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import utils.jira_xray.XRayCucumberFeatureFetcher;
import utils.jira_xray.XRayCucumberResultsImporter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class JiraStepdefs {
    List<String> ids = new ArrayList<>();
    File outputDir;

    @Given("feature fetch parameters are defined")
    public void featureFetchParametersAreDefined() {
        ids.add("TAAADEV-484");
        outputDir = new File("src/test/java/practice01/resources/jira");
    }

    @When("a request to jira xray is sent")
    public void aRequestToJiraXrayIsSent() {
        XRayCucumberFeatureFetcher featureFetcher = new XRayCucumberFeatureFetcher();
        featureFetcher.fetch(ids, outputDir);
    }

    @Then("feature file is received")
    public void featureFileIsReceived() throws IOException {
        Assert.assertTrue("No files received", Files.list(Paths.get("src/test/java/prectice01/practice01.resources/jira")).findAny().isPresent());
    }

    @Given("test result file is present")
    public void testResultFileIsPresent() throws IOException {
        Assert.assertTrue("No files received", Files.list(Paths.get("target/cucumber-html-reports")).findAny().isPresent());
    }

    @When("test result file to jira xray is sent")
    public void testResultFileToJiraXrayIsSent() {
        File resultsFile = new File("target/cucumber-html-reports/cucumber.json");
        XRayCucumberResultsImporter resultsImporter = new XRayCucumberResultsImporter();
        resultsImporter.importResults(resultsFile);
    }

    @Then("the results are successfully imported")
    public void theResultsAreSuccessfullyImported() {

    }
}
