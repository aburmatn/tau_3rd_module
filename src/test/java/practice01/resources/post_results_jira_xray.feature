Feature: Import feature results to Jira XRay

  Scenario: Successful feature result import
    Given test result file is present
    When test result file to jira xray is sent
    Then the results are successfully imported
