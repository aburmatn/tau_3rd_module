package practice01;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/practice01/resources/jira/1.feature",
        plugin = {"json:target/cucumber-html-reports/cucumber.json", "pretty"}
)
public class RunPractice01Test {

}
