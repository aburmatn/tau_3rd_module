package practice01;

import com.codeborne.selenide.Condition;
import org.junit.Test;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PropertyLoader;

import static com.codeborne.selenide.Condition.appears;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.url;


public class Practice01Test extends TestBase  {

    protected Logger log = LoggerFactory.getLogger(getClass());

    @Test
    public void openGoodLoginPage(){

        log.info("page is: " + url());
        $(By.id("guest_signin_button")).waitUntil(appears, 10000);
        $(By.id("guest_signin_button")).click();
        $(By.cssSelector("[name=IDToken1]")).val(PropertyLoader.loadProperty("email"));
        $(By.cssSelector("[name=btnOK]")).click();
        log.info("page is: " + url());
        $(By.xpath("//span[contains(text(),'MyPortal Account (Alternative)')]")).click();
        $(By.cssSelector("#username")).val(PropertyLoader.loadProperty("email"));
        $(By.xpath("//input[@id='password']")).val(PropertyLoader.loadProperty("pass"));
        $(By.cssSelector("#submit")).shouldBe(Condition.visible).click();


        $(By.cssSelector("[title='Meetings']")).click();
        $(By.cssSelector("#scheduleEntry")).shouldBe(Condition.visible).click();
        $(By.cssSelector("#topic")).shouldBe(Condition.exist);
        $(By.cssSelector("#topic")).clear();
        $(By.cssSelector("#topic")).shouldHave(exactText(""));
        $(By.cssSelector("#topic")).val("super_puper_Meeting");

        $(By.xpath("//a[@id='scheduleTimeTrigger']/i")).click();
        $(By.xpath("//*[@aria-label='Tuesday April 20, 2021']")).click();
        $(By.cssSelector("#scheduleDone")).click();
        $(By.cssSelector("#invite_attendee_input")).val("elena.sergeeva@t-systems.com");
        $(By.cssSelector("#scheduleEdit")).click();

        log.info("page is: " + url());

    }

}
