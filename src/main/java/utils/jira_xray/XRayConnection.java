package utils.jira_xray;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.not;


public class XRayConnection {

    private final String hostUri;
    private final String username;
    private final String password;

    private final String CUCUMBER_FEATURES_EXPORT_ENDPOINT = "rest/raven/1.0/export/test";
    private final String CUCUMBER_RESULTS_IMPORT_ENDPOINT = "rest/raven/1.0/import/execution/cucumber";
    private final String CUCUMBER_RESULTS_NEW_ISSUE_IMPORT_ENDPOINT = "rest/raven/1.0/import/execution/cucumber/multipart";

    public XRayConnection() {
        this.hostUri = "https://jira.devops.telekom.de";
        this.username = "taaa-robot";
        this.password = "u4upGpiKmKn8";
    }

    public Response fetchFeatureFiles(List<String> ids) {
        return given(new RequestSpecBuilder().
                setBaseUri(hostUri + '/' + CUCUMBER_FEATURES_EXPORT_ENDPOINT).
                addParam("keys", String.join(";", ids)).
                addParam("fz", "true").
                addFilter(new RequestLoggingFilter()).
                addFilter(new ResponseLoggingFilter(not(200))).
                addFilter(new ResponseLoggingFilter(LogDetail.STATUS, System.out, 200)).
                setRelaxedHTTPSValidation().
                build()).
                auth().preemptive().basic(username, password).
                contentType(ContentType.JSON).
                get();
    }

    public Response postCucumberResultFiles(String results) {
        return given(new RequestSpecBuilder().
                setBaseUri(hostUri + '/' + CUCUMBER_RESULTS_IMPORT_ENDPOINT).
                setBody(results).
                addFilter(new RequestLoggingFilter()).
                addFilter(new ResponseLoggingFilter()).
                setRelaxedHTTPSValidation().
                build()).
                auth().preemptive().basic(username, password).
                contentType(ContentType.JSON).
                post();
    }

    public Response postCucumberResultFilesToNewIssue(String results) {
        return given(new RequestSpecBuilder().
                setBaseUri(hostUri + '/' + CUCUMBER_RESULTS_NEW_ISSUE_IMPORT_ENDPOINT).
                addMultiPart("info", "", "application/json").
                addMultiPart("result", results, "application/json").
                addFilter(new RequestLoggingFilter()).
                addFilter(new ResponseLoggingFilter()).
                build()).
                auth().preemptive().basic(username, password).
                contentType(ContentType.JSON).
                post();
    }
}
