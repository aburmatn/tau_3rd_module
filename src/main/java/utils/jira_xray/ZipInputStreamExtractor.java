package utils.jira_xray;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Slf4j
public class ZipInputStreamExtractor {

    private final File outputDir;

    public ZipInputStreamExtractor(File outDir) {
        if (outDir.exists()) {
            try {
                Files.walk(outDir.toPath())
                        .sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .forEach(File::delete);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        outDir.mkdirs();
        outputDir = outDir;
    }

    public void extract(ZipInputStream zipInputStream) {
        try {
            byte[] buffer = new byte[2048];
            ZipEntry ze;
            while ((ze = zipInputStream.getNextEntry()) != null) {
                try {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    int length;
                    while ((length = zipInputStream.read(buffer)) != -1) {
                        os.write(buffer, 0, length);
                    }
                    try (FileOutputStream fos = new FileOutputStream(
                            Files.createFile(outputDir.toPath().resolve(ze.getName())).toFile())) {
                        os.writeTo(fos);
                    }
                } catch (IOException e) {
//                    log.error("IOException while saving feature file " + ze.getName());
                }
            }
        } catch (IOException e) {
//            log.error("IOException while saving feature files", e);
        }
    }
}
