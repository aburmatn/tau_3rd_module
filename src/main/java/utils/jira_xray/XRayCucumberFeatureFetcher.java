package utils.jira_xray;

import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipInputStream;

@Slf4j

public class XRayCucumberFeatureFetcher {
    private XRayConnection connection;

    public XRayCucumberFeatureFetcher() {
        this.connection = new XRayConnection();
    }

    public void fetch(List<String> ids, File outputDir) {
        Response response = connection.fetchFeatureFiles(ids);

        if (response.getStatusCode() == 200) {
            try (InputStream fis = response.body().asInputStream();
                 BufferedInputStream bis = new BufferedInputStream(fis);
                 ZipInputStream zis = new ZipInputStream(bis)) {

                ZipInputStreamExtractor fw = new ZipInputStreamExtractor(outputDir);
                fw.extract(zis);
            } catch (IOException e) {
              //  log.error("IOError while run test: ", e);
                throw new RuntimeException("Unable to communicate with Jira: " + response.getStatusLine());
            }
        } else {
        //    log.error("Unable to communicate with Jira: " + response.getStatusLine());
            throw new RuntimeException("Unable to communicate with Jira: " + response.getStatusLine());
        }
    }
}
