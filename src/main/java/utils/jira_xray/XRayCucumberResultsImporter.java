package utils.jira_xray;

import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Slf4j
public class XRayCucumberResultsImporter {
    private XRayConnection connection;

    public XRayCucumberResultsImporter() {
        this.connection = new XRayConnection();
    }

    public void importResults(File source) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(source.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Response response = connection.postCucumberResultFiles(content);

        if (response.getStatusCode() != 200) {
 //           log.error("Unable to communicate with Jira: " + response.getStatusLine());
            throw new RuntimeException("Unable to communicate with Jira: " + response.getStatusLine());
        }
    }
}
